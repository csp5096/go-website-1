// +build mem

package model

type Connection = bool
type Key = int64

func Open(options ...string) (bool, error) {
	// there should be something here connecting to memory
	return true, nil
}
