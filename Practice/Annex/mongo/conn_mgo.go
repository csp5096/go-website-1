// +build mgo

package data

import (
	"./model"
	"./mongo"
)

func (db *DB) Open(driverName, dataSourceName string) error {
	conn, err := model.Open(driverName, dataSourceName)
	if err != nil {
		return err
	}
	// for mongo, we need to copy this connection session at each requests
	// this is done in our api's ServeHTTP
	// db.CopySession = true
	db.Users = &mongo.Users{}

	// initialize services (see this later)
	db.Connection = conn
	return nil
}
