// +build mem

package mem

import (
	"../model"
)

// Users - array of User models
type Users struct {
	store []model.User
}

// GetDetail - get a user given an id
func (u *Users) GetDetail(id model.Key) (*model.User, error) {
	var user model.User

	for _, usr := range u.store {
		if usr.ID == id {
			user = usr
			break
		}
	}
	return &user, nil
}

// RefreshSession - refresh a user session
func (u *Users) RefreshSession(conn *bool, dbName string) {
	u.store = append(u.store, model.User{ID: 1, Email: "test@domain.com"})
}
