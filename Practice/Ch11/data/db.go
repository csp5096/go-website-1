package data

import (
	"./model"
)

type DB struct {
	DatabaseName string
	Connection   *model.Connection
	CopySession  bool

	Users UserServices
}

type SessionRefresher interface {
	RefreshSession(*model.Connection, string)
}

type UserServices interface {
	SessionRefresher
	GetDetail(id model.Key) (*model.User, error)
	SignUp(email, password string) (*model.Account, error)
	AddToken(accountId, userID model.Key, name string) (*model.AccessToken, error)
	RemoveToken(accountID, userID, tokenID model.Key) error
	GetDetail(id model.Key) (*model.Account, error)
}

type AdminServices interface {
	SessionRefresher
	LogRequests(reqs []model.APIRequest) error
}
