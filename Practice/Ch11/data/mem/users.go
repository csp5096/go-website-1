// +build mem

package mem

import (
	"fmt"

	"../model"
)

// Users - array of User models
type Users struct {
	store []model.User
}

// GetDetail - get a user given an id
func (u *Users) GetDetail(id model.Key) (*model.User, error) {
	var user model.User

	for _, usr := range u.store {
		if usr.ID == id {
			user = usr
			break
		}
	}

	return &user, nil
}

// RefreshSession - refresh a user session
func (u *Users) RefreshSession(conn *bool, dbName string) {
	u.store = append(u.store, model.User{ID: 1, Email: "test@domain.com"})
}

func (u *Users) AddToken(accountID, userID model.Key, name string) (*model.AccessToken, error) {
	tok := model.AccessToken{
		ID:    userID * 300,
		Name:  name,
		Token: model.NewToken(accountID),
	}

	for _, acct := range u.users {
		if acct.ID == accountID {
			for _, usr := range acct.Users {
				if usr.ID == user.ID {
					usr.AccessTokens = append(usr.AccessTokens, tok)
					return &tok, nil
				}
			}
		}
	}

	return nil, fmt.Errorf("unable to find account %d and user %d", accountID, userID)
}

func (u *Users) RemoveToken(accountID, userID, tokenID, model.Key) error {
	for _, acct := range u.users {
		if acct.ID == accountID {
			for _, usr := range acct.Users {
				if usr.ID == userID {
					// cheap by does not need more here :p
					usr.AccessTokens = make([]model.AccessToken, 0)
					break
				}
			}
		}
	}
	return nil
}
