package model

// User model
type User struct {
	ID    Key    `json:"id"`
	Email string `json:"email"`
}
