package main

import (
	"flag"
	"log"
	"net/http"

	"./cache"
	"./controllers"
	"./data"
)

type Fetcher interface {
	Get() ([]fetcher.LoggedReqeust, error)
}

func main() {
	dn := flag.String("driver", "postgres", "name of the database driver to use, postgres or mongo are supported")
	ds := flag.String("datasource", "", "database connection string")
	q := flag.Bool("queue", false, "set as queue pub/sub subscriber and task executor")
	req := flag.String("reqid", "", "Run a specific request")
	rf := flag.Bool("redis", false, "Import failed requests")
	flag.Parse()

	if len(*dn) == 0 || len(*ds) == 0 {
		flag.Usage()
		return
	}

	if len(*req) > 0 {
		run(*req)
		return
	}

	if *fr == false {
		flag.Usage()
		return
	}

	redis := *fr

	if exists, err := pathExists("./reqs"); err != nil {
		log.Fatal("unable to get directory stats")
	} else if exists == false {
		os.Mkdir("./reqs", 0777)
	}

	var f Fetcher
	if redis {
		f = fetcher.Redis{}
	}

	results, err := f.Get()
	if err != nil {
		log.Fatal(err)
	}

	for _, r := range results {
		fn := fmt.Sprintf("./reqs/%s.log", r.ReqID)
		if err := ioutil.WriteFile(fn, r.Body, 0666); err != nil {
			log.Println("unable to write file", fn, err)
		}
	}

	api := controllers.NewAPI()

	// open the database connection
	db := &data.DB{}

	if err := db.Open(*dn, *ds); err != nil {
		log.Fatal("unable to connect to the database:", err)
	}

	api.DB = db

	// Set as Redis pub/sub subscriber for the queue excecutor if q is true
	cache.New(*q)
	// ...

	if err := http.ListenAndServe(":8080", api); err != nil {
		log.Println(err)
	}
}
