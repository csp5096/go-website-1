package model

type Account struct {
	ID    Key    `json:"id"`
	Email string `json:"email"`
	Users []User `json:"users"`
}

type User struct {
	ID Key `json:"id"`
	// AccountID Key `json:"-"`
	Email        string        `json:"email"`
	Password     string        `json:"-"`
	Token        string        `json:"token"`
	Role         Roles         `json:"role"`
	AccessTokens []AccessToken `json:"accessTokens"`
}

type AccessToken struct {
	ID    Key    `json:"id"`
	Name  string `json:"name"`
	Token string `json:"token"`
}

type Roles int

const (
	RoleAdmin Roles = iota
	RoleUser
)
