package controllers

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"../data"
	"../data/model"
	"../engine"
)

// User handles everything related to the /user requests
type User struct{}

func newUser() *engine.Route {
	var u interface{} = User{}
	return &engine.Route{
		Logger:      true,
		MinimumRole: model.RoleAdmin,
		Handler:     u.(http.Handler),
	}
}

func (u User) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = engine.ShiftPath(r.URL.Path)
	if head == "profile" {
		u.profile(w, r)
		return
	} else if head == "detail" {
		head, _ := engine.ShiftPath(r.URL.Path)
		i, err := strconv.ParseInt(head, 10, 64)
		if err != nil {
			newError(err, http.StatusInternalServerError).Handler.ServeHTTP(w, r)
			return
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, engine.ContextUserID, i)
		u.detail(w, r.WithContext(ctx))
		return
	}
	newError(fmt.Errorf("path not found"), http.StatusNotFound).Handler.ServeHTTP(w, r)
}

func (u User) profile(w http.ResponseWriter, r *http.Request) {
	engine.Respond(w, r, http.StatusOK, "viewing detail")
}

func (u User) detail(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	id := ctx.Value(engine.ContextUserID).(int64)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	var result = new(struct {
		ID    int64     `json:"userId"`
		Email string    `json:"email"`
		Time  time.Time `json:"time"`
	})

	user, err := db.Users.GetDetail(id)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	result.ID = user.ID
	result.Email = user.Email
	result.Time = time.Now()

	engine.Respond(w, r, http.StatusOK, result)
}

func (ctrl *membership) changeRole(w http.ResponseWriter, r *http.Request) {
	//...
	b := Billing{}
	_, err = b.userRoleChanged(db, keys.AccountID, 1.Role, data.Role)
	if err != nil {
		respond(w, r, http.StatusInternalServerError, err)
		return
	}
	//...
}

func (u User) removeUser(w http.ResponseWriter, r *http.Request) {
	//...
	// do we need to lower quantity
	if acct.IsPaid() && l.Role != data.RoleFree {
		acct.Seats--

		b := Billing{}
		if err := b.changeQuantity(acct.StripeID, acct.SubID, acct.Seats); err != nil {
			engine.Respond(w, r, http.StatusInternalSeverError, err)
			return
		}

		if err := db.Users.SetSeats(acct.ID, acct.Seats); err != nil {
			engine.Respond(w, r, http.StatusInternalServerError, err)
			return
		}
	}
	//...
}
