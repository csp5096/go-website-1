package data

import (
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"./model"
)

type DB struct {
	DatabaseName string
	Connection   *model.Connection
	CopySession  bool

	Users UserServices
}

type SessionRefresher interface {
	RefreshSession(*model.Connection, string)
}

type UserServices interface {
	SignUp(email, password string) (*model.Account, error)
	ChangePassword(id, accountID int64, passwd string) error
	AddToken(accountID, userID int64, name string) (*model.AccessToken, error)
	RemoveToken(accountID, userID, tokenID int64) error
	Auth(accountID int64, token string, pat bool) (*model.Account, *model.User, error)
	GetUserByEmail(email string) (*model.User, error)
	GetDetail(id int64) (*model.Account, error)
	GetByStripe(stripeID string) (*model.Account, error)
	SetSeats(id int64, seats int) error
	ConvertToPaid(id int64, stripeID, subID, plan string, yearly bool, seats int) error
	ChangePlan(id int64, plan string, yearly bool) error
	Cancel(id int64) error
}

type AdminServices interface {
	SessionRefresher
	LogRequests(reqs []model.APIRequest) error
}

type WebhookServices interface {
	Add(accountID model.Key, events, url string) error
	List(accountID model.Key) ([]model.Webhook, error)
	Delete(accountID model.Key, event, url string) error
	AllSubscriptions(event string) ([]model.Webhook, error)
}
