package model

import (
	"time"
)

type Roles int

const (
	RoleAdmin Roles = iota
	RoleUser
)

type Account struct {
	ID    Key    `json:"id"`
	Email string `json:"email"`
	Users []User `json:"users"`
}

type User struct {
	ID Key `json:"id"`
	// AccountID Key `json:"-"`
	Email        string        `json:"email"`
	Password     string        `json:"-"`
	Token        string        `json:"token"`
	Role         Roles         `json:"role"`
	AccessTokens []AccessToken `json:"accessTokens"`
}

type AccessToken struct {
	ID    Key    `json:"id"`
	Name  string `json:"name"`
	Token string `json:"token"`
}

// APIRequest represents a single API call
type APIRequest struct {
	ID         Key       `json:"id"`
	AccountID  Key       `json:"accountId"`
	UserID     Key       `json:"userId"`
	URL        string    `json:"url"`
	Requested  time.Time `json:"requested"`
	StatusCode int       `json:"statusCode"`
	RequestID  string    `json:"reqId"`
}

type Webhook struct {
	ID        Key       `json:"id"`
	AccountID Key       `json:"accountId"`
	EventName string    `json:"event"`
	TargetURL string    `json: "targetUrl"`
	isAction  bool      `json:"active"`
	Created   time.Time `json:"created"`
}
