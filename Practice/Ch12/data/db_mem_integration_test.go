// +build !mgo
// +build integration

package data

import (
	"testing"
)

func TestDBOpen(t *testing.T) {
	db := DB{}
	if err := db.Open("redis", "127.0.0.1"); err != nil {
		t.Fatal("unable to connect to redis", err)
	}
}
