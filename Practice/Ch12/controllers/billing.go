package controllers

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"../data"
	"../data/model"
	"../engine"

	// "../queue"
	"github.com/dstpierre/gosaas/queue"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/card"
	"github.com/stripe/stripe-go/customer"
	"github.com/stripe/stripe-go/invoice"
	"github.com/stripe/stripe-go/sub"
)

// BillingNewCustomer represents data sent to api for creating a new customer
type BillingNewCustomer struct {
	Plan     string          `json:"plan"`
	Card     BillingCardData `json:"card"`
	Coupon   string          `json:"coupon"`
	Zip      string          `json:"zip"`
	IsYearly bool            `json:"yearly"`
}

// BillingOverview represents if an account is a paid customer or not
type BillingOverview struct {
	StripeID       string            `json:"stripeId"`
	Plan           string            `json:"plan"`
	IsYearly       bool              `json:"isYearly"`
	IsNew          bool              `json:"isNew"`
	Cards          []BillingCardData `json:"cards"`
	CostForNewUser int               `json:"costForNewUser"`
	CurrentPlan    *dal.BillingPlan  `json:"currentPlan"`
	Seats          int               `json:"seats"`
	Logins         []dal.Login       `json:"logins"`
}

// BillingCardData represents a Stripe credit card
type BillingCardData struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Number     string `json:"number"`
	Month      string `json:"month"`
	Year       string `json:"year"`
	CVC        string `json:"cvc"`
	Brand      string `json:"brand"`
	Expiration string `json:"expiration"`
}

// Billing handles everything related to the billing requests
type Billing struct{}

func init() {
	stripe.Key = os.Getenv("STRIPE_KEY")
}

func newBilling() *engine.Route {
	var b interface{} = User{}
	return &engine.Route{
		Logger:      true,
		MinimumRole: model.RoleAdmin,
		Handler:     b.(http.Handler),
	}
}

func (b Billing) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.PATH = engine.ShiftPath(r.URL.PATH)
	if r.Method == http.MethodGet {
		if head == "overview" {
			b.overview(w, r)
		} else if head == "invoices" {
			head, r.URL.Path = engine.ShiftPath(r.URL.Path)
			if head == "" {
				b.invoices(w, r)
			} else if head == "next" {
				b.getNextInvoice(w, r)
				return
			}
		} else if r.Method == http.MethodPost {
			if head == "start" {
				b.start(w, r)
			} else if head == "changeplan" {
				b.changePlan(w, r)
			}
		}
	}
}

func (b Billing) overview(w http.ResponseWriter, r *http.Request) {
	// check if this account is paid or not
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	// this struct will be returned regardless of
	// whether or not we are a paid customer
	ov := BillingOverview{}

	// Get the current account
	account, err := db.Accounts.Get(keys.AccountID)
	if err != nil {
		// should not publicly disclose the error - bad
		engine.Respond(w, r, http.StatusNotFound, err)
		return
	}

	// Getting logins and calculating paying seats
	ov.Logins = account.Users

	// get all logins for user roles
	for _, l := range account.Users {
		if l.Role < model.RoleFree {
			ov.Seats++
		}
	}

	if len(account.StripeID) == 0 {
		ov.IsNew = true
		// if they are on trial, we set the current plan to
		// that so the UI can base permissions on that plan.
		if account.TrialInfo.IsTrial {
			if p, ok := data.GetPlan(account.TrialInfo.Plan); ok {
				ov.CurrentPlan = &p
			}

			engine.Respond(w, r, http.StatusOK, ov)
			return
		}
	}

	// paid customer code next...

	// getting stripe customer
	cus, err := customer.Get(account.StripeID, nil)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	ov.StripeID = cus.ID
	ov.Plan = account.Plan
	ov.IsYearly = account.IsYearly

	if p, ok := data.GetPlan(account.Plan); ok {
		ov.CurrentPlan = &p
	}

	cards := card.List(&stripe.CardListParams{Customer: account.StripeID})
	for cards.Next() {
		c := cards.Card()
		if !c.Deleted {
			ov.Cards = append(ov.Cards, BillingCardData{
				ID:         c.ID,
				Name:       c.Name,
				Number:     c.LastFour,
				Month:      fmt.Sprintf("%d", c.Month),
				Year:       fmt.Sprintf("%d", c.Year),
				Expiration: fmt.Sprintf("%d / %d", c.Month, c.Year),
				Brand:      string(c.Brand),
			})
		}
	}

	enigne.Respond(w, r, http.StatusOK, ov)
}

func (b Billing) changeQuantity(stripeID, subID string, qty int) error {
	p := &stripe.SubParams{Customer: stripeID, Quantity: uint64(qty)}
	_, err := sub.Update(subID, p)
	return err
}

func (b Billing) userRoleChanged(db data.DB, accountID model.Key, oldRole, newRole model.Roles) (paid bool, err error) {
	acct, err := db.Users.Get(accountID)
	if err != nil {
		return false, err
	}

	// if this is a paid account
	if acct.IsPaid() {
		// if they were a free user
		if oldRole == model.RoleFree {
			// and are now a paid user, we need to +1 qty and prepare the invoice
			if newRole == model.RoleAdmin || newRole == model.RoleUser {
				paid = true
				// we increase the seats' number for this account
				acct.Seats++
				// try to change their subscription (+1 qty)
				if err = b.changeQuantity(acct.StripeID, acct.SubID, acct.Seats); err != nil {
					return
				}

				// ensure that the charges wil be immediate
				// and not on next billing date (create a Stripe Invoice)
				// when a Stripe charge is tirggered
				// when a subscription quantity increases
				if err := queue.Enqueue(queue.TaskCreateInvoice, acct.StripeID); err != nil {
					return paid, err
				}

				if err = db.UsersSetSeats(acct.ID, acct.Seats); err != nil {
					return false, err
				}
			}
		} else {
			// they were a paid user, now they are set as free
			if newRole == model.RoleFree {
				acct.Seats--
				if err = b.changeQuantity(acct.StripeID, acct.SubID, acct.Seats); err != nil {
					return
				}

				if err = db.Users.SetSeats(acct.ID, acct.Seats); err != nil {
					return false, err
				}
			}
		}
	}

	return false, nil
}

// Billing function that will be called when a queued job is processed
func (b *Billing) Run(qt QueueTask) error {
	id, ok := qt.Data.(string)
	if !ok {
		return fmt.Errorf("the data should be a stripee customer ID")
	}

	// we delay execution for 2 hours to let add/remove
	// operations in between creating the invoice
	// since we're on a go routine we can use a time.Sleep
	time.Sleep(2 * time.Hour)

	p := &stripe.InvoiceParams{Customer: id}
	_, err := invoice.New(p)
	return err
}

// Create the Stripe customer with the
// captured credit card as a payment source
func (b Billing) start(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	var data BillingNewCustomer
	if err := engine.ParseBody(r.Body, &data); err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	p := &stripe.CustomerParams{Email: keys.Email}
	p.SetSource(&stripe.CardParams{
		Name:   data.Card.Name,
		Number: data.Card.Number,
		Month:  data.Card.Month,
		Year:   data.Card.Year,
		CVC:    data.Card.CVC,
		Zip:    data.Zip,
	})

	c, err := customer.New(p)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalSeverError, err)
		return
	}

	// Get the desired plan and create a subscription
	acct, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	seats := 0
	for _, u := range acct.User {
		if u.Role < model.RoleFree {
			seats++
		}
	}

	plan := data.Plan
	if data.IsYearly {
		plan += "_yearly"
	}

	// Coupon: "PRELAUNCH1"
	subp := &stripe.SubParams{
		Customer: c.ID,
		Plan:     plan,
		Quantity: uint64(seats),
	}

	if len(data.Coupon) > 0 {
		subp.Coupon = data.Coupon
	}

	s, err := sub.New(subp)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	// We have a new paying customer with an active subscription
	// Time to change their plan and cancel their trial state
	acct.TrialInfo.IsTrial = false
	if err := db.Users.ConvertToPaid(acct.ID, c.ID, s.ID, data.Plan, data.IsYearly, seats); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	ov := BillingOverview{}
	ov.StripeID = c.ID
	ov.Plan = data.Plan
	ov.IsYearly = data.IsYearly
	ov.Seats = seats

	acct.StripeID = c.ID
	acct.SubscribedOn = time.Now()
	acct.SubID = s.ID
	acct.Plan = data.Plan
	acct.IsYearly = data.IsYearly
	acct.Seats = seats

	engine.Respond(w, r, http.StatusOK, ov)
}

func (b Billing) changePlan(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	var data = new(struct {
		Plan string `json:"plan"`
		isYearly bool `json:"isYearly"`
	})
	if err := engine.ParseBody(r.Body, &data); err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	plan := data.Plan
	
	newLevel, currentLevel := 0, 0
	if len(plan) == 0 || plan == "free" {
		newLevel = 0
	} else if strings.HasPrefix(plan, "starter") {
		newLevel = 1
	} else if strings.HasPrefix(plan, "pro") {
		newLevel = 2
	} else {
		newLevel = 3
	}

	if strings.HasPrefix(account.Plan, "starter") {
		currentLevel = 1
	} else if strings.HasPrefix(account.Plan, "pro") {
		currentLevel = 2
	} else {
		currentLevel = 3
	}

	// did they cancelled
	if newLevel == 0 {
		// we need to cancel their subscriptions
		if _, err := sub.Cancel(account.SubId, nil); err != nil {
			engine.Respond(w, r, http.StatusInternalServerError, err)
			return
		}

		if err := accounts.Cancel(account.ID); err != nil {
			engine.Respond(w, r, http.StatusInternalServerError, err)
			return
		}
	} else {
		if data.isYearly {
			plan += "_yearly"
		}

		// calculate paid users
		// skip for clarity, we have this same code already at multiple places.
		// Time for a refactor by the way of honor DRY prinicle.

		subParams := &stripe.SubParams{
			Customer: account.StripeID,
			Plan: plan,
			Quantity: uint64(seats)
		}

		// If we upgrade we need to change billing cycle date
		upgarded := false
		if newLevel > currentLevel {
			ugrapded = true
		} else if account.IsYearly == false && data.isYearly {
			upgraded = true
		}

		if upgraded {
			// queue an invoice create for this upgrade
			queue.Enqueue(queue.TaskCreateInvoice, account.StripeID)
		}

		if _, err := sub.Update(account.SubID, subParams); err != nil {
			engine.Respond(w, r, http.StatusInternalServerError, err)
			return
		}
		//...
	}
}

func (b Billing) updateCard(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	var data BillingCardData
	if err := engine.ParseBody(r.Body, &data); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	if c, err := card.Update(data,ID,
	&stripe.CardParams{Customer: account.StripeID, 
	Month: data.Month, Year: data.Year, CVC: data.CVC}); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
	} else {
		card := BillingCardData {
			ID:         c.ID,
			Name:       c.Name,
			Number:     c.LastFour,
			Month:      fmt.Sprintf("%d", c.Month),
			Year:       fmt.Sprintf("%d", c.Year),
			Expiration: fmt.Sprintf("%d / %d", c.Month, c.Year),
			Brand:      string(c.Brand),
		}
		engine.Respond(w, r, http.StatusOK, card)
	}
}

func (b Billing) addCard(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	var data BillingCardData
	if err := engine.ParseBody(r.Body, &data); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	if c, err := card.New(
		&stripe.CardParams{Customer: account.StripeID, 
		Month: data.Month, Year: data.Year, CVC: data.CVC}); err != nil {
			engine.Respond(w, r, http.StatusInternalServerError, err)
		} else {
			card := BillingCardData{
				ID:         c.ID,
				Number:     c.LastFour,
				Expiration: fmt.Sprintf("%d / %d", c.Month, c.Year),
				Brand:      string(c.Brand),
			}
			engine.Respond(w, r, http.StatusOK, card)
		}
}

func (b Billing) deleteCard(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	cardID := mux.Vars(r)["id"]

	if _, err := card.Del(cardID, &stripe.CardParams{Customer: account.StripeID}); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
	} else {
		engine.Respond(w, r, http.StatusOK, true)
	}
}

func (b Billing) invoices(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	var invoices []*stripe.Invoice

	iter := invoice.List(&stripe.InvoiceListParams{Customer:account.StripeID})
	for iter.Next() {
		invoices = append(invoices, iter.Invoice())
	}

	engine.Respond(w, r, http.StatusOK, invoices)
}

func (b Billing) getNextInvoice(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	i, err := invoice.GetNext(&stripe.InvoiceParams{Customer:account.StripeID})
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	engine.Respond(w, r, http.StatusOK, invoices)
}

func (b Billing) stripe(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	// no mater what happen, Stripe wants us to send a 200
	defer w.Write([]byte("ok"))

	var data WebhookData
	if err := engine.ParseBody(r.Body, &data); err != nil {
		log.Println(err)
		return
	}

	if data.Type == "customer.subscription.deleted" {
		subID := data.Data.Object.ID
		if len(subID) == 0 {
			log.Println(fmt.Errorf("no subscription found
			to this customer.subscription.deleted %s", data.ID))
			return
		}

		stripeID := data.Data.Object.Customer
		if len(stripeID) == 0 {
			log.Println(fmt.Errorf("no cutomer found to
			this invoice.payment_succeeded %s", data.ID))
			return
		}

		// check if it's a failed payment_succeeded
		account, err := db.Users.GetByStripe(stripeID)
		if err != nil {
			log.Println(fmt.Errorf("no customer matches stripe id", stripeID))
			return
		}

		if len(account.SubscriptionID) > 0 {
			// Send emails

			if err := db.Users.Cancel(accoun.ID); err != nil {
				log.Println(fmt.Errorf("unable to cancel this account", account.ID))
				return
			}
		}
	}
}

func (b Billing) cancel(w http.ResponseWriter, r *http.Request) {
	// FYI - this is bad practice any dependency should be passed
	// explicitly as a parameter to a function and not through
	// context inside of a function
	ctx := r.Context()
	keys := ctx.Value(engine.ContextAuth).(engine.Auth)
	db := ctx.Value(engine.ContextDatabase).(*data.DB)

	var data = new(struct {
		Reason string `json:"reason"`
	})
	if err := engine.ParseBody(r.Body, &data); err != nil {
		engine.Respond(w, r, http.StatusBadRequest, err)
		return
	}

	// SendMail would be called here passing the reason
	account, err := db.Users.GetDetail(keys.AccountID)
	if err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	if _, err := sub.Cancel(account.SubscriptionID, nil); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	if err := db.Users.Cancel(account.ID); err != nil {
		engine.Respond(w, r, http.StatusInternalServerError, err)
		return
	}

	engine.Respond(w, r, http.StatusOK, true)
}