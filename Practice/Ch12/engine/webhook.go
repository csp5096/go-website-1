package engine

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"../data"
	"../data/model"
)

func Post(url string, data interface{}, result interface{}, headers map[string]string) error {
	b, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewReader(b))
	if err != nil {
		return nil
	}

	if headers != nil && len(headers) > 0 {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("failed post, error: ", err)
		} else {
			log.Println("failed post, recv: ", string(b))
		}
		return fmt.Errorf("error requesting %s returned %s", url, resp.Status)
	}

	if result != nil {
		decode := json.NewDecoder(resp.Body)
		if err := decoder.Decode(result); err != nil {
			return err
		}
	}
	return nil
}

// SendWebhook posts data to all subscribers of an event.
func SendWebhook(wh data.WebhookServices, event string, data interface{}) {
	headers := make(map[string]string)
	headers["X-Webhook-Event"] = event

	subscribers, err := wh.AllSubscriptions(event)
	if err != nil {
		log.Println("unable to get webhook subscribers for ", event)
		return
	}

	for _, sub := range subscribers {
		go func(sub model.Webhook, headers map[string]string) {
			if err := post(sub.TargetURL, data, nil, headers); err != nil {
				log.Println("error calling URL", sub.TargetURL, err)
			}
		}(sub, headers)
	}
}
