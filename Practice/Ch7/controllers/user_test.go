package controllers

import (
	"net/http"
	"testing"
)

func TestUserProfileHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/user/profile", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := executeRequest(req)
	if status := rec.Code; status != http.StatusOK {
		t.Errorf("returns status %v was expecintg %v", status, http.StatusOK)
	}
}
